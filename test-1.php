<?php
function primeCheck($numb): bool
{
    if ($numb == 1) {
        return false;
    }

    if ($numb == 2) {
        return true;
    }

    $sqrtNumb = sqrt($numb);
    $floorNumb = floor($sqrtNumb);

    for ($i = 2; $i <= $floorNumb; ++$i) {
        if ($numb % $i == 0) {
            break;
        }
    }

    if ($floorNumb == $i - 1) {
        return true;
    }

    return false;
}

?>