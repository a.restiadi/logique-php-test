<?php
function biggestNumber($numbers) : int {
    $biggest = 0;


    foreach ($numbers as $number) {
        if ($number > $biggest) {
            $biggest = $number;
        }
    }


    return $biggest;
}

?>