<?php
function sortNumber($numbers) : array {
    do {
        $swapped = false;


        for ($i = 0, $c = count($numbers) - 1; $i < $c; $i++) {
            if ($numbers[$i] > $numbers[$i + 1]) {
                list($numbers[$i + 1], $numbers[$i]) =
                    array($numbers[$i], $numbers[$i + 1]);
                $swapped = true;
            }
        }
    } while ($swapped);


    return $numbers;
}


$numbers = [99, 2, 64, 8, 111, 33, 65, 11, 102, 50];


echo implode(', ', sortNumber($numbers));
?>